<?php
namespace Application\Sonata\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PostAdmin extends Admin
{
    public $sonata_admin = 'default name';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
            ->add('enabled', null, array('required' => false))
            ->add('author', 'sonata_type_model_list', array(
                'btn_add'       => 'Add author',      //Specify a custom label
                'btn_list'      => 'button.list',     //which will be translated
                'btn_delete'    => false,             //or hide the button.
                'btn_catalogue' => 'SonataNewsBundle' //Custom translation domain for buttons
            ), array(
                'placeholder' => 'No author selected'
            ))
            ->add('title')
            ->add('abstract')
            ->add('content')
            ->end()
            ->with('Tags')
            ->add('tags', 'sonata_type_model', array('expanded' => true))
            ->end()
            ->with('Options', array('collapsed' => true))
            ->add('commentsCloseAt')
            ->add('commentsEnabled', null, array('required' => false))
            ->add('commentsDefaultStatus', 'choice', array('choices' => Comment::getStatusList()))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('enabled')
            ->add('tags', null, array(), null, array('expanded' => true, 'multiple' => true))
            ->add('author')
            ->add('with_open_comments', 'doctrine_orm_callback', array(
//                'callback'   => array($this, 'getWithOpenCommentFilter'),
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }

                    $queryBuilder->leftJoin(sprintf('%s.comments', $alias), 'c');
                    $queryBuilder->andWhere('c.status = :status');
                    $queryBuilder->setParameter('status', Comment::STATUS_MODERATE);

                    return true;
                },
                'field_type' => 'checkbox'
            ))
        ;
    }

    public function getWithOpenCommentFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        $queryBuilder->leftJoin(sprintf('%s.comments', $alias), 'c');
        $queryBuilder->andWhere('c.status = :status');
        $queryBuilder->setParameter('status', Comment::STATUS_MODERATE);

        return true;
    }

    public function create($object)
    {
        $object->setPadre($this->getModelManager()->findOne('CarlaCmsBundle:Contenido', $this->getRequest()->get('padre')));

        $this->prePersist($object);
        $this->modelManager->create($object);
        $this->postPersist($object);
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setName('my default value');

        return $instance;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('slug')
            ->add('author')
        ;
    }
}