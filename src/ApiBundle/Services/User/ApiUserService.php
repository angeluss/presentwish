<?php

namespace ApiBundle\Services\User;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use ApiBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ApiUserService
{
    /**
     * @var $em EntityManager
     */
    protected $em;

    /**
     * @var $container Container
     */
    protected $container;

    /**
     * User constructor.
     * @param Container $container
     * @param EntityManager $em
     */
    public function __construct(Container $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function createUser($data)
    {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setUsername($data['email']);
        $user->setPlainPassword($data['password']);

        $this->em->persist($user);
        $this->em->flush();

        $this->authenticateUser(array(
            'email' => $data['email'],
            'password' => $data['password'],
        ));
    }

    public function authenticateUser($data)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserBy(array(
            'email' => $data['email'],
        ));
        if ($user) {
            $encoder_service = $this->container->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);
            $isValid = $encoder->isPasswordValid($user->getPassword(), $data['password'], $user->getSalt());
            if ($isValid) {
                $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
                $context = $this->container->get('security.context');
                $context->setToken($token);
                return true;
            }
        }

        return false;
    }

    public function addFunds($data){
        if ( $data['user'] ) {
            $user = $data['user'];
            if( $this->add() ) { //@TODO paymentSystem
                $user->setAmount($user->getAmount() + $data['amount']);

                $this->em->persist($user);
                $this->em->flush();
                return true;
            }
        }
        return false;

    }

    public function withdrawFunds($data){
        $result = ['success' => false, 'error' => ''];

        if ( $data['user'] ) {
            $user = $data['user'];

            if( $this->withdraw() ) {//@TODO paymentSystem
                $this->withdraw();
                $user->setAmount($user->getAmount() - $data['amount']);

                if($user->getAmount() >= 0) {
                    $this->em->persist($user);
                    $this->em->flush();
                    $result['success'] =  true;
                } else {
                    $result['error'] = 'Insufficient funds';
                }

            } else {
                $result['error'] = 'Can\'t withdraw funds. Please, try again later';
            }

        } else {
            $result['error'] = 'Can\'t find user';
        }

        return $result;
    }

    protected function withdraw(){
        return true;
    }

    protected function add(){
        return true;
    }
}