<?php

namespace ApiBundle\Services\Social;

use Symfony\Component\DependencyInjection\Container;
use ApiBundle\Services\User\ApiUserService;

class ApiSocialService
{
    /**
     * @var $userService User
     */
    protected $userService;

    /**
     * @var $container Container
     */
    protected $container;

    public function __construct(ApiUserService $user, Container $container)
    {
        $this->userService = $user;
        $this->container = $container;
    }

    public function createGoogleUser($token)
    {
        $userInfo = $this->getGoogleUserInfo($token);
        if ($userInfo) {
            $userInfo['social_platform'] = 'google';
            $this->userService->createSocialUser($userInfo);
            return true;
        }

        return false;
    }

    protected function getGoogleUserInfo($token)
    {
        $client = new \Google_Client();
        $client->setApplicationName($this->container->getParameter('google_plus_app_name'));
        $client->setDeveloperKey($this->container->getParameter('google_plus_key'));
        $ticket = $client->verifyIdToken($token);
        if ($ticket) {
            return array(
                'email' => $ticket['email'],
                'social_id' => $ticket['sub']
            );
        }

        return false;
    }

    public function createFacebookUser()
    {
        try {
            $fb = new \Facebook\Facebook([
                'app_id' => "{$this->container->getParameter('facebook_app_id')}",
                'app_secret' => "{$this->container->getParameter('facebook_app_secret')}",
                'default_graph_version' => 'v2.2',
            ]);

            $helper = $fb->getJavaScriptHelper();

            $accessToken = $this->getFacebookAccessToken($helper, $fb);

            $userData = $this->getFacebookUserData($accessToken, $fb);

            $userInfo = array(
                'email' => $userData['email'],
                'social_id' => $userData['id'],
                'social_platform' => 'facebook',
            );

            $this->userService->createSocialUser($userInfo);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    protected function getFacebookUserData($accessToken, $fb)
    {
        try {
            $response = $fb->get('/me?fields=id,name,email', $accessToken);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        return $response->getGraphUser();
    }

    protected function getFacebookAccessToken($helper)
    {
        try {
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            echo 'No cookie set or no OAuth data could be obtained from cookie.';
            exit;
        }

        return $accessToken->getValue();
    }

//    public function createVkontakteUser($userId)
//    {
//        $ch = curl_init();
//
//        $urlParams = array(
//            'api_id' => $this->container->getParameter('vkontakte_app_id');
//            'method'
//        );
//        curl_setopt($ch, CURLOPT_URL, "http://api.vkontakte.ru/api.php");
//        curl_setopt($ch, CURLOPT_HEADER, 0);
//
//        curl_exec($ch);
//
//        curl_close($ch);
//    }
}