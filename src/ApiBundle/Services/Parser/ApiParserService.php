<?php

namespace ApiBundle\Services\Parser;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class ApiParserService
{
    /**
     * @var $url string
     */
    protected $url;

    /**
     * @var $data mixed
     */
    protected $data;

    /**
     * User constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    public function parseMT()
    {
        $result = get_meta_tags($this->url, false);

        return $result;
    }

    public function file_get_contents_curl($url)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_POST, false);
        curl_setopt($handle, CURLOPT_BINARYTRANSFER, false);
        curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);


        $data = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if ($httpCode != 200) {
            $data = false;
        }

        return $data;
    }

    public function parseOG()
    {
        $result = [];
        $metas = '';
        $html = $this->file_get_contents_curl($this->url);
        if(!$html){
            return $result;
        }

        $pattern = '
          ~<\s*meta\s
        
          # using lookahead to capture type to $1
            (?=[^>]*?
            \b(?:property|http-equiv)\s*=\s*
            (?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|
            ([^"\'>]*?)(?=\s*/?\s*>|\s\w+\s*=))
          )
        
          # capture content to $2
          [^>]*?\bcontent\s*=\s*
            (?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|
            ([^"\'>]*?)(?=\s*/?\s*>|\s\w+\s*=))
          [^>]*>
        
          ~ix';

        if(preg_match_all($pattern, $html, $out))
            $metas = array_combine($out[1], $out[2]);


        foreach ($metas as $name => $content)
        {
            if($name == 'og:description')
                $result['description'] = $content;
            if($name == 'og:title')
                $result['title'] = $content;
            if($name == 'og:image')
                $result['image'] = $content;
            if($name == 'og:price')
                $result['price'] = $content;
        }

        return $result;
    }

    public function parse()
    {
        $this->data = [
            'title' => '',
            'description' => '',
            'price' => '',
            'image' => '',
        ];

        $curl_result = $this->parseOG();
        if(!$curl_result){
            return false;
        }
        $result = $this->parseMT();

        $fin = false;
        foreach ($this->data as $k => $v){
            if(isset($result[$k])){
                $this->data[$k] = $result[$k];
            } else {
                $fin = true;
            }
        }


        foreach ($this->data as $k => $v){
            if(isset($curl_result[$k]) && $v === ''){
                $this->data[$k] = $curl_result[$k];
            }
        }

        return $this->data;
    }

    public function putImg(){
        $photo_info = pathinfo($this->data['image']);

        if( $photo_info['extension'] == "" || $photo_info['basename'] == "" || !in_array($photo_info['extension'], array('png','jpg','jpeg','gif')) ) {
            return false;
        }
//        $path = $this->getRootDir();
        $newfile = md5(microtime()."_".$photo_info['basename']).".".$photo_info['extension'];
//        $uploaded_photo = $path . "/uploads/tmp/" . $newfile;
        $uploaded_photo = "/tmp/" . $newfile;

        $ch = curl_init($this->data['image']);
        $fp = fopen($uploaded_photo, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        if(!file_exists($uploaded_photo)) {
            return false;
        }

        $file =  new UploadedFile( $uploaded_photo, $newfile, 'image/jpeg',null,null,true);

        return $file;
    }

    protected function getRootDir(){
        return dirname(__FILE__) . '/../../../../web';
    }

}