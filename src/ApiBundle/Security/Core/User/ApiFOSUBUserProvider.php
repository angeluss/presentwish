<?php

namespace ApiBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;

class ApiFOSUBUserProvider extends BaseFOSUBProvider
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $serviceName = $response->getResourceOwner()->getName();
        $username = $response->getUsername();

        $existingUser = $this->userManager->findUserBy(array($property => $username));
        if (null !== $existingUser) {
            $existingUser->{'set' . ucfirst($serviceName) . 'Id'}(null);
            $existingUser->{'set' . ucfirst($serviceName) . 'AccessToken'}(null);

            $this->userManager->updateUser($existingUser);
        }
        $user->{'set' . ucfirst($serviceName) . 'Id'}($response->getUsername());
        $user->{'set' . ucfirst($serviceName) . 'AccessToken'}($response->getAccessToken());
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $userEmail = $response->getEmail();
        $user = $this->userManager->findUserByEmail($userEmail);
        $serviceName = $response->getResourceOwner()->getName();

        // if null just create new user and set it properties
        if (null === $user) {
            $username = $response->getRealName();
            $user = new User();
            $user->setUsername($username);
            $user->setEmail($response->getEmail());
            $user->setPlainPassword(md5(uniqid()));
            $user->setEnabled(true);
            $user->{'set' . ucfirst($serviceName) . "Id"}($response->getUsername());
            $user->{'set' . ucfirst($serviceName) . "AccessToken"}($response->getAccessToken());
            $this->userManager->updateUser($user);

            return $user;
        }
        // else update access token of existing user
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        $user->$setter($response->getAccessToken());//update access token

        return $user;
    }
}