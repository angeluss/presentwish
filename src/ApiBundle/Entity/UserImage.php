<?php
namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_image")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\Repository\UserImageRepository")
 */
class UserImage
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $changed_at;

    /**
     * @ORM\OneToOne(cascade={"persist"}, targetEntity="User", inversedBy="image")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToOne(cascade={"persist"}, targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     */
    private $media;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * uses for form
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $getFrom;

    const TYPE_AVATAR = 1;
    const GET_FROM_URL = 1;
    const GET_FROM_FILE = 2;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return UserImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set changedAt
     *
     * @param \DateTime $changedAt
     *
     * @return UserImage
     */
    public function setChangedAt($changedAt)
    {
        $this->changed_at = $changedAt;

        return $this;
    }

    /**
     * Get changedAt
     *
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changed_at;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return UserImage
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return UserImage
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set getFrom
     *
     * @param integer $getFrom
     *
     * @return UserImage
     */
    public function setGetFrom($getFrom)
    {
        $this->getFrom = $getFrom;

        return $this;
    }

    /**
     * Get getFrom
     *
     * @return integer
     */
    public function getGetFrom()
    {
        return $this->getFrom;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return UserImage
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
