<?php
namespace ApiBundle\Entity;

use AppBundle\Form\Type\Goal\CreateGoalFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="goal")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\Repository\GoalRepository")
 */
class Goal {

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $access;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_finish;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $receiver;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $insta_stat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vk_stat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fb_stat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $twitter_stat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ok_stat;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="goals")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(cascade={"persist", "remove"}, targetEntity="GoalImage", mappedBy="goal")
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity="GoalTarget", inversedBy="goals")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $target;

    /**
     * @ORM\OneToMany(cascade={"persist", "remove"}, targetEntity="MoneyLog", mappedBy="goal")
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $moneyLogs;

    const FOR_ME = 1;
    const FOR_FRIEND = 2;

    const VK_REF = 'vk.com';
    const FB_REF = 'facebook.com';
    const INSTA_REF = 'instagram.com';
    const OK_REF = 'ok.ru';
    const TWIT_REF = 'twitter.com';

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Goal
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Goal
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Goal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Goal
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set access
     *
     * @param integer $access
     *
     * @return Goal
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     * @return integer
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Get access name
     *
     * @return string
     */
    public function getAccessName()
    {
        $accesses = [
            CreateGoalFormType::PUBLIC_ACCESS => 'goal.public_access',
            CreateGoalFormType::PROTECTED_ACCESS => 'goal.protected_access',
            CreateGoalFormType::PRIVATE_ACCESS => 'goal.private_access',
        ];
        return $accesses[$this->access];
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Goal
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Goal
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return Goal
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \ApiBundle\Entity\GoalImage $image
     *
     * @return Goal
     */
    public function addImage(\ApiBundle\Entity\GoalImage $image)
    {
        $image->setGoal($this);
        $image->setCreatedAt(new \DateTime('now'));
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \ApiBundle\Entity\GoalImage $image
     */
    public function removeImage(\ApiBundle\Entity\GoalImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set images
     *
     * @return Goal
     */
    public function setImages(\Doctrine\Common\Collections\Collection $images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * Set date_finish
     *
     * @param \DateTime $date_finish
     *
     * @return Goal
     */
    public function setDateFinish($date_finish)
    {
        $this->date_finish = $date_finish;

        return $this;
    }

    /**
     * Get date_finish
     *
     * @return \DateTime
     */
    public function getDateFinish()
    {
        return $this->date_finish;
    }

    /**
     * Set receiver
     *
     * @param integer $receiver
     *
     * @return Goal
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     * @return integer
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Get receiver
     * @return integer
     */
    public function getReceiverTitle()
    {
        $receivers = array(
            self::FOR_ME => 'goal.for_me',
            self::FOR_FRIEND => 'goal.for_friend',
        );
        return $receivers[$this->receiver];
    }

    /**
     * Set insta_stat
     *
     * @param integer $insta_stat
     *
     * @return Goal
     */
    public function setInstaStat($insta_stat)
    {
        $this->insta_stat = $insta_stat;

        return $this;
    }

    /**
     * Get insta_stat
     * @return integer
     */
    public function getInstaStat()
    {
        return $this->insta_stat;
    }


    /**
     * Set vk_stat
     *
     * @param integer $vk_stat
     *
     * @return Goal
     */
    public function setVkStat($vk_stat)
    {
        $this->vk_stat = $vk_stat;

        return $this;
    }

    /**
     * Get vk_stat
     * @return integer
     */
    public function getVkStat()
    {
        return $this->vk_stat;
    }

    /**
     * Set fb_stat
     *
     * @param integer $fb_stat
     *
     * @return Goal
     */
    public function setFbStat($fb_stat)
    {
        $this->fb_stat = $fb_stat;

        return $this;
    }

    /**
     * Get fb_stat
     * @return integer
     */
    public function getFbStat()
    {
        return $this->fb_stat;
    }

    /**
     * Set twitter_stat
     *
     * @param integer $twitter_stat
     *
     * @return Goal
     */
    public function setTwitterStat($twitter_stat)
    {
        $this->twitter_stat = $twitter_stat;

        return $this;
    }

    /**
     * Get twitter_stat
     * @return integer
     */
    public function getTwitterStat()
    {
        return $this->twitter_stat;
    }

    /**
     * Set ok_stat
     *
     * @param integer $ok_stat
     *
     * @return Goal
     */
    public function setOkStat($ok_stat)
    {
        $this->ok_stat = $ok_stat;

        return $this;
    }

    /**
     * Get ok_stat
     * @return integer
     */
    public function getOkStat()
    {
        return $this->ok_stat;
    }

    /**
     * Set target
     *
     * @param \ApiBundle\Entity\GoalTarget $target
     *
     * @return Goal
     */
    public function setTarget(\ApiBundle\Entity\GoalTarget $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \ApiBundle\Entity\GoalTarget
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Add moneyLog
     *
     * @param \ApiBundle\Entity\MoneyLog $moneyLog
     *
     * @return Goal
     */
    public function addMoneyLog(\ApiBundle\Entity\MoneyLog $moneyLog)
    {
        $moneyLog->setGoal($this);
        $moneyLog->setCreatedAt(new \DateTime('now'));
        $this->moneyLogs[] = $moneyLog;

        return $this;
    }

    /**
     * Remove moneyLog
     *
     * @param \ApiBundle\Entity\MoneyLog $moneyLog
     */
    public function removeMoneyLog(\ApiBundle\Entity\MoneyLog $moneyLog)
    {
        $this->moneyLogs->removeElement($moneyLog);
    }

    /**
     * Get moneyLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoneyLogs()
    {
        return $this->moneyLogs;
    }

    public static function createFromWish($wish, $em){
        $goal = new self;
        $goal->setTitle($wish->getTitle());
        $goal->setUrl($wish->getUrl());
        $goal->setDescription($wish->getDescription());
        $goal->setPrice($wish->getPrice());
        $goal->setAccess(CreateGoalFormType::PRIVATE_ACCESS);
        $goal->setCreatedAt($wish->getCreatedAt());
        $goal->setModifiedAt($wish->getModifiedAt());
        $goal->setUser($wish->getUser());
        $goal->setVkStat(0);
        $goal->setFbStat(0);
        $goal->setInstaStat(0);
        $goal->setTwitterStat(0);
        $goal->setOkStat(0);
        $goal->setDateFinish(new \DateTime(date('Y-m-d', strtotime("+30 days"))));

        return $goal;
    }

    /**
     * @param $wish Wish
     * @param $ids array
     */
    public function setImagesFromWish($wish, $ids){
        foreach($wish->getImages() as $wishImg){
            if(is_null($ids) || !in_array($wishImg->getId(), $ids) ) {
                $image = new GoalImage();
                $image->setGoal($this);
                $image->setUrl($wishImg->getUrl());
                $image->setMedia($wishImg->getMedia());
                $image->setCreatedAt(new \DateTime('now'));

                $this->addImage($image);
            }
        }
    }

    public function getLeftTime(){
        $interval = $this->getDateFinish()->diff(new \DateTime('now'));
        echo $interval->format('%a');
    }

    public function getAmount($type = null){
        $amount = 0;
        foreach ($this->getMoneyLogs() as $moneyLog){
            $amount += $moneyLog->getAmount();
        }
        switch($type){
            case 'percent':
                if( $amount < $this->price){
                    return $amount * 100 / $this->price;
                } else {
                    return 100;
                }
                break;
            case 'partial':
                if( $amount < $this->price){
                    return $amount / $this->price;
                } else {
                    return 100;
                }
            default:
                return $amount;
        }
    }

    public function getMoneyLeft(){
        $amount = $this->getAmount();
        if($amount < $this->price){
            return $this->price - $amount;
        } else {
            return 0;
        }
    }

    /**
     * @param $ref - referer
     */
    public function setStatistics($ref){
        if(stristr($ref, self::FB_REF) != FALSE) {
            $this->fb_stat++;
        }
        if(stristr($ref, self::VK_REF) != FALSE) {
            $this->vk_stat++;
        }
        if(stristr($ref, self::INSTA_REF) != FALSE) {
            $this->insta_stat++;
        }
        if(stristr($ref, self::TWIT_REF) != FALSE) {
            $this->twitter_stat++;
        }
        if(stristr($ref, self::OK_REF) != FALSE) {
            $this->ok_stat++;
        }
    }

}
