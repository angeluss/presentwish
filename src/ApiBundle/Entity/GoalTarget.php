<?php

namespace ApiBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="goal_target")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\Repository\GoalTargetRepository")
 */
class GoalTarget
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $type;

    /**
     * @ORM\OneToMany(cascade={"persist"}, targetEntity="Goal", mappedBy="target")
     */
    private $goals;

    const PUBLIC_ACCESS = 1;
    const PRIVATE_ACCESS = 2;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return GoalTarget
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set type
     *
     * @param integer $type
     *
     * @return GoalTarget
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get type name
     *
     * @return string
     */
    public function getTypeName()
    {
        $types = [
            self::PUBLIC_ACCESS => 'goal_target.public_access',
            self::PRIVATE_ACCESS => 'goal_target.private_access',
        ];
        return $types[$this->type];
    }

    /**
     * Add goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     *
     * @return GoalTarget
     */
    public function addGoal(Goal $goal)
    {
        $this->goals[] = $goal;

        return $this;
    }

    /**
     * Remove goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     */
    public function removeGoal(Goal $goal)
    {
        $this->goals->removeElement($goal);
    }

    /**
     * Get goals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoals()
    {
        return $this->goals;
    }

    public function __toString() {
        return $this->title;
    }
}