<?php
namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="money_log")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\Repository\MoneyLogRepository")
 */
class MoneyLog
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(cascade={"persist"}, targetEntity="Goal", inversedBy="moneyLogs")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $goal;

    /**
     * @ORM\ManyToOne(cascade={"persist"}, targetEntity="User", inversedBy="moneyLogs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $status;

    const PUBLIC_ACCESS = 1;
    const PRIVATE_ACCESS = 2;

    const STATUS_OPEN = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAIL = 3;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return MoneyLog
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return MoneyLog
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return MoneyLog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get type name
     *
     * @return string
     */
    public function getTypeName()
    {
        $types = [
            self::PUBLIC_ACCESS => 'goal_money_log.public_access',
            self::PRIVATE_ACCESS => 'goal_money_log.private_access',
        ];
        return $types[$this->type];
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MoneyLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     *
     * @return MoneyLog
     */
    public function setGoal(\ApiBundle\Entity\Goal $goal = null)
    {
        $this->goal = $goal;
        return $this;
    }

    /**
     * Get goal
     *
     * @return \ApiBundle\Entity\Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return MoneyLog
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return MoneyLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
