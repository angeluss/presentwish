<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    protected $facebookId;

    protected $facebookAccessToken;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $googleId;

    protected $googleAccessToken;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $vkontakteId;

    protected $vkontakteAccessToken;

    /**
     * @ORM\OneToMany(targetEntity="Wish", mappedBy="user")
     */
    private $wishes;

    /**
     * @ORM\OneToMany(targetEntity="Goal", mappedBy="user")
     */
    private $goals;

    /**
     * @ORM\OneToMany(cascade={"persist", "remove"}, targetEntity="MoneyLog", mappedBy="user")
     */
    private $moneyLogs;

    /**
     * @ORM\OneToOne(cascade={"persist", "remove"}, targetEntity="UserImage", mappedBy="user")
     */
    private $image;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $amount;

    public function __construct()
    {
        parent::__construct();
        $this->users = new ArrayCollection();
    }

    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->googleAccessToken;
    }

    /**
     * @param string $googleAccessToken
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->googleAccessToken = $googleAccessToken;

        return $this;
    }

    /**
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    public function getVkontakteId()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * @param string $vkontakteAccessToken
     * @return User
     */
    public function setVkontakteAccessToken($vkontakteAccessToken)
    {
        $this->vkontakteAccessToken = $vkontakteAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteAccessToken()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * Add wish
     *
     * @param \ApiBundle\Entity\Wish $wish
     *
     * @return User
     */
    public function addWish(\ApiBundle\Entity\Wish $wish)
    {
        $this->wishes[] = $wish;

        return $this;
    }

    /**
     * Remove wish
     *
     * @param \ApiBundle\Entity\Wish $wish
     */
    public function removeWish(\ApiBundle\Entity\Wish $wish)
    {
        $this->wishes->removeElement($wish);
    }

    /**
     * Get wishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWishes()
    {
        return $this->wishes;
    }

    /**
     * Add goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     *
     * @return User
     */
    public function addGoal(Goal $goal)
    {
        $this->goals[] = $goal;

        return $this;
    }

    /**
     * Remove goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     */
    public function removeGoal(Goal $goal)
    {
        $this->goals->removeElement($goal);
    }

    /**
     * Get goals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Add moneyLog
     *
     * @param \ApiBundle\Entity\MoneyLog $moneyLog
     *
     * @return User
     */
    public function addMoneyLog(\ApiBundle\Entity\MoneyLog $moneyLog)
    {
        $this->moneyLogs[] = $moneyLog;

        return $this;
    }

    /**
     * Remove moneyLog
     *
     * @param \ApiBundle\Entity\MoneyLog $moneyLog
     */
    public function removeMoneyLog(\ApiBundle\Entity\MoneyLog $moneyLog)
    {
        $this->moneyLogs->removeElement($moneyLog);
    }

    /**
     * Get moneyLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoneyLogs()
    {
        return $this->moneyLogs;
    }

    /**
     * Set image
     *
     * @param \ApiBundle\Entity\UserImage $image
     *
     * @return User
     */
    public function setImage(\ApiBundle\Entity\UserImage $image = null)
    {
        $this->image = $image;
        $image->setUser($this);
        $image->getType($image::TYPE_AVATAR);
        $image->setGetFrom($image::GET_FROM_FILE);

        return $this;
    }

    /**
     * Get image
     *
     * @return \ApiBundle\Entity\UserImage
     */
    public function getImage()
    {
        return $this->image;
    }

    public function getDefaultAvatar(){
        return "https://d3lut3gzcpx87s.cloudfront.net/res/img/UnknownProfile.png";
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return User
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
