<?php
namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="goal_images")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\Repository\GoalImageRepository")
 */
class GoalImage
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(cascade={"persist"}, targetEntity="Goal", inversedBy="images")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $goal;

    /**
     * @ORM\OneToOne(cascade={"persist"}, targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     */
    private $media;

    /**
     * @var integer
     * uses for form
     */
    public $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return GoalImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return GoalImage
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set goal
     *
     * @param \ApiBundle\Entity\Goal $goal
     *
     * @return GoalImage
     */
    public function setGoal(\ApiBundle\Entity\Goal $goal = null)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return \ApiBundle\Entity\Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return GoalImage
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param $wishImage WishImage
     * @param $goal Goal
     * @param $em EntityManager
     * @return GoalImage
     */
    public static function createFromWishImage($wishImage, $goal, $em){
        $media = clone $wishImage->getMedia();
        $media->setId(null);
        $em->persist($media);
        $em->flush();
        if($media->getId() == 113) die;
        $image = new self;
        $image->setGoal($goal);
        $image->setMedia($media);
        $image->setCreatedAt(new \DateTime('now'));
        return $image;
    }
}
