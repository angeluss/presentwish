<?php
namespace ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class WishImageRepository extends EntityRepository{

    /**
     * get all WishImages
     * @return array
     */
    public function getAll() {
//        $qb = $this->createQueryBuilder('a')
//            ->select('a, u')
//            ->innerJoin('a.user', 'u');
//
//        return $qb->getQuery()->getResult();
    }

    /**
     * Get all WishImages depending on media
     * @param $media_id
     * @return array
     */
    public function getAllForMedia($media_id) {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->innerJoin('a.media', 'u')
            ->where('(u.id = :media)')
            ->setParameter('media', $media_id);

        return $qb->getQuery()->getResult();
    }
}