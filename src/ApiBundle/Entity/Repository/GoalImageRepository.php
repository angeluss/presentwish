<?php
namespace ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class GoalImageRepository extends EntityRepository{

    /**
     * get all GoalImages
     * @return array
     */
    public function getAll() {

    }

    /**
     * Get all GoalImages depending on goal
     * @param $goal_id
     * @return array
     */
    public function getAllForGoal($goal_id) {

    }

    /**
     * Get all GoalImages depending on media
     * @param $media_id
     * @return array
     */
    public function getAllForMedia($media_id) {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->innerJoin('a.media', 'u')
            ->where('(u.id = :media)')
            ->setParameter('media', $media_id);

        return $qb->getQuery()->getResult();
    }
}