<?php
namespace ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class MoneyLogRepository extends EntityRepository{

    /**
     * get all GoalImages
     * @return array
     */
    public function getAll() {

    }

    /**
     * Get all GoalImages depending on goal
     * @param $goal_id
     * @return array
     */
    public function getAllForGoal($goal_id) {

    }
}