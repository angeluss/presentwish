<?php
namespace ApiBundle\Entity\Repository;

use ApiBundle\Entity\GoalTarget;
use Doctrine\ORM\EntityRepository;

class GoalTargetRepository extends EntityRepository{

    /**
     * get all GoalTarget
     * @return array
     */
    public function getAll() {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->innerJoin('a.goals', 'u');

        return $qb->getQuery()->getResult();
    }

    public function getAllPublic(){
        $qb = $this->createQueryBuilder('a');
        $qb ->select('a')
            ->where($qb->expr()->like('a.type', '?1'))
            ->setParameter(1, GoalTarget::PUBLIC_ACCESS);

        return $qb->getQuery()->getResult();
    }
}