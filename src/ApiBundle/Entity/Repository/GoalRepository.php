<?php
namespace ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class GoalRepository extends EntityRepository{

    /**
     * get all Wishes
     * @return array
     */
    public function getAll() {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->innerJoin('a.user', 'u');

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all Wishes depending on user
     * @param $user_id
     * @return array
     */
    public function getAllForUser($user_id) {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->innerJoin('a.user', 'u')
            ->where('(u.id = :user)')
            ->setParameter('user', $user_id);

        return $qb->getQuery()->getResult();
    }
}