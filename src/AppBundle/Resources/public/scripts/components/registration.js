var registration = {
    init: function(baseUrl) {
        this.validateRegistrationForm();
        this.signupFormAction();
        this.resetPassword();
    },

    validateRegistrationForm: function(){
        var registrationForm = $('#registration_form');
        registrationForm.submit(function(event){
            event.preventDefault();
            $.ajax({
                url: Routing.generate('validate_registration_form'),
                method: "POST",
                data: registrationForm.serialize(),
                beforeSending: function(){

                },
                success: function(data) {
                    if (data.success === false && data.errors !== undefined) {
                        var lengthOfFileds = 0;
                        data.errors.forEach(function(error){
                            var field = $(registrationForm).find('input[name*="registration_form[' + error.field + ']"]');
                            field.parent().addClass('has-error');
                            lengthOfFileds++;
                            if (field.length == lengthOfFileds) {
                                field.last().after($('<span class="help-inline has-error">' + error.message + '</span>'));
                                lengthOfFileds = 0;
                            }
                        })
                    } else {
                        $(registrationForm).children('.has-error').removeClass('.has-error');
                        window.location.reload(true);
                    }
                }
            });
        });
    },

    signupFormAction: function() {
        var signUpForm = $('#signup_form');
        signUpForm.submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: Routing.generate('sign_up_action'),
                method: "POST",
                data: signUpForm.serialize(),
                beforeSending: function(){

                },
                success: function(data) {
                    if (data.success === false) {
                        if ($('.email-or-password-error').length === 0) {
                            signUpForm.parent().append($('<span class="email-or-password-error">' + data.message + '</span>'))
                        }
                    } else {
                        $('.email-or-password-error').remove();
                        // window.location.reload(true);
                        window.location = Routing.generate('wishes');
                    }
                }
            });
        });
    },

    resetPassword: function() {
        var resetPasswordForm = $('#resetPassword');
        resetPasswordForm.submit(function(event) {
            event.preventDefault();

            $.ajax({
                url: resetPasswordForm.attr('action'),
                method: "POST",
                data: resetPasswordForm.serialize(),
                beforeSending: function(){

                },
                success: function(data) {
                    if (data.success === false) {
                        $('#resetting_error').css('display', 'block').css('color', 'red').html(data.message);
                        $('#username').css('border-color', 'red');
                        // resetPasswordForm.parent().append($('<span class="reset-password-error">' + data.message + '</span>'))
                    } else {
                        $('#resetting_error').css('display', 'none').html('');
                        resetPasswordForm.css('display', 'none');
                        $('#resetting_success').css('display', 'block').html(data.message);
                    }
                }
            });
        });
    }
}