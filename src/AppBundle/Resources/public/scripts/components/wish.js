$( document ).ready(function(){
    var body = $('body');
    function getWishData(urlInput){
        var link = $(urlInput).val();
        var url = $('.createWishForm-container').data('url');
        var titleInput = $('#create_wish_form_title');
        var descriptionInput = $('#create_wish_form_description');
        var priceInput = $('#create_wish_form_price');
        var descriptionEditor = $('#wish-description-editor');
        var form = descriptionEditor.closest('form');

        $.ajax({
            url: url,
            method: "POST",
            data: ({link : link}),
            beforeSending: function(){

            },
            success: function(data) {
                if(data.success){
                    titleInput.val(data.title);
                    //
                    // var $metapic = $('#metapic');
                    // if($metapic.length == 0) {
                    //     var $collectionHolder;
                    //     var linkName = $('.add_tag_link').html();
                    //     // setup an "add a tag" link
                    //     var $addImageLink = $('<a href="#" class="add_tag_link">' + linkName + '</a>');
                    //     var $newLinkLi = $('<li></li>').append($addImageLink);
                    //     $('.images li:last').remove();
                    //     // Get the ul that holds the collection of tags
                    //     $collectionHolder = $('ul.images');
                    //
                    //     // add the "add a tag" anchor and li to the tags ul
                    //     $collectionHolder.append($newLinkLi);
                    //
                    //     // count the current form inputs we have (e.g. 2), use that as the new
                    //     // index when inserting a new item (e.g. 2)
                    //     $collectionHolder.data('index', $collectionHolder.find(':input').length);
                    //
                    //     // add a new tag form (see next code block)
                    //     addImageForm($collectionHolder, $newLinkLi);
                    //
                    //     $addImageLink.on('click', function (e) {
                    //         // prevent the link from creating a "#" on the URL
                    //         e.preventDefault();
                    //
                    //         // add a new tag form (see next code block)
                    //         addImageForm($collectionHolder, $newLinkLi);
                    //     });
                    //     $metapic = $('.images').find('li:last').prev();
                    //     $metapic.attr('id', 'metapic');
                    // } else {
                    //
                    // }
                    // $metapic.find('.image_type').hide();
                    // $metapic.find('input[type=text]').val(data.image).show();
                    // $metapic.find('input[type=file]').hide();
                    form.append('<input id="media_id" type="hidden" name="media_id" value="' + data.media + '" />');
                    dz.emit("addedfile", data.preview);
                    dz.emit("thumbnail", data.preview, data.preview['thumb']);
                    dz.emit("complete", data.preview);

                    $( ".wish-files .dz-preview" ).last().addClass( "create-img-parser" ).children('.dz-remove').attr('onclick', 'removeParserImage();');

                    descriptionInput.val(data.description);
                    descriptionEditor.textContent = data.description;
                    descriptionEditor.empty().append(data.description);
                    priceInput.val(data.price);
                }
            }
        });
    }

    $('#create_wish_form_url').on('change', function(){
        getWishData(this);
    });
    body.on('change', '.image_type input[type=radio]', function(){
        var val = $(this).val();
        var viaUrl = $(this).closest('li').find('.text_block');
        var viaFile = $(this).closest('li').find('.media_block');
        if(val == 2){
            viaUrl.css('display', 'none');
            viaFile.css('display', 'block');
        } else if(val == 1){
            viaUrl.css('display', 'block');
            viaFile.css('display', 'none');
        }
    });
    $('.delete_wishimage_button').on('click', function(){
        var result = confirm("Want to delete?");
        if (result) {
            var id = $(this).data('id');
            var li = $(this).closest('li');
            $.ajax({
                url: $(this).closest('ul'). data('url'),
                method: "POST",
                data: ({id : id}),
                beforeSending: function(){

                },
                success: function(data) {
                    if(data.success){
                        li.remove();
                    }
                }
            });
        }

    });

    $(document).mouseup(function (e)
    {
        var container = $("#wysiwyg-card");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('#wysiwyg-editor').hide();
        }
    });

    $('#create_wish_form').on('submit', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var data = new FormData();
        $.each(dz.getQueuedFiles(), function(i, file) {
            data.append('files[]', file);
        });

        var form = $(this).serializeArray();
        $.each(form, function (i, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                if(data.status == 'success') {
                    location.href = Routing.generate('wishes');
                }
            }
        });
    });

    $('#add_wish').on('click', function(){
        location.href = $(this).data('url');
    });

    $('.wish_item_container').on('click', function(){
        location.href = $( this ).data('url');
    });
    /* view and show sharing link script */
    $('.sharing_link').click(
        function() {
            if ($('#tmp').length) {
                $('#tmp').remove();
            }
            var clickText = $(this).text();
            $('<textarea id="tmp" />')
                .appendTo($(this))
                .val(clickText)
                .focus()
                .select();
            return false;
        }
    );
    $(':not(.sharing_link)').click(
        function(){
            $('#tmp').remove();
        }
    );
    /* end view and show sharing link script */


});
function removeParserImage() {
    $('#media_id').remove();
}