$(document).ready(function(){
    $('body').on('change', '.image_type input[type=radio]', function(){
        var val = $(this).val();
        var viaUrl = $('#fos_user_profile_form_image_url');
        var viaFile = $('#fos_user_profile_form_image_media');
        if(val == 2){
            viaUrl.css('display', 'none');
            viaFile.css('display', 'block');
        } else if(val == 1){
            viaUrl.css('display', 'block');
            viaFile.css('display', 'none');
        }
    });
    $('#profile_avatar_change_button').on('click', function(){
        $(this).hide();
        $('.profile-edit-avatar-block').show();
    });

    $("#first").focus();

    $("#second").on('click',function(){
        $(this).toggleClass('checked-default');
        $('#first').toggleClass('checked-default');
    });

    $('#first').on('click',function(){
        $(this).toggleClass('checked-default');
        $('#second').toggleClass('checked-default');
    });

    $('label[for="fos_user_profile_form_image_media_unlink"]').hide();
    $('label[for="fos_user_profile_form_image_media_binaryContent"]').hide();

});