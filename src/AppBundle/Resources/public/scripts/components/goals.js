function fakeLoading($obj, speed, failAt) {
    if (typeof speed == "undefined") speed = 2;
    if (typeof failAt == "undefined") failAt = -1;
    var v = 0;
    var l = function () {
        if (failAt > -1) {
            if (v >= failAt) {
                if (typeof $obj.jquery != "undefined") {
                    $obj.ElasticProgress("failed");
                } else {
                    $obj.fail();
                }
                return;
            }
        }
        v += Math.pow(Math.random(), 2) * 0.1 * speed;

        if (typeof $obj.jquery != "undefined") {
            $obj.ElasticProgress("setValue", v);
        } else {
            $obj.setValue(v);
        }
        if (v < 1) {
            TweenMax.delayedCall(0.05 + (Math.random() * 0.14), l)
        }
    };
    l();
}

$(document).ready(function () {
    var $el = $('.collected');
    $.each($el, function( index, value ) {
        var e = new ElasticProgress(value, {
            fontFamily: "Roboto",
            colorFg: "#77c2ff",
            colorBg: "#4e80dd",
            highlightColor: "#ed7499",
            barHeight: 5,
            barInset: 5,
            textFail: $(value).data('collected-full') + " grn",
            width: $(value).parent().width() - 15
        });
        e.open();
        e.onOpen(function () {
            fakeLoading(e, 2, $(value).data('collected-partial'));
        });
    });

    $(document).mouseup(function (e)
    {
        var container = $("#wysiwyg-card");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('#wysiwyg-editor').hide();
        }
    });
    /* view and show sharing link script */
    $('.sharing_link').click(
        function() {
            if ($('#tmp').length) {
                $('#tmp').remove();
            }
            var clickText = $(this).text();
            $('<textarea id="tmp" />')
                .appendTo($(this))
                .val(clickText)
                .focus()
                .select();
            return false;
        }
    );
    $(':not(.sharing_link)').click(
        function(){
            $('#tmp').remove();
        }
    );
    /* end view and show sharing link script */

    /* view and show showmore button script */
    var size_tr = $("#moneyLogs tr").size();
    var x=3;
    var show_more = $('#loadMore');
    $('#moneyLogs tr:lt('+x+')').show();
    show_more.click(function () {
        x= (x+3 <= size_tr) ? x+3 : size_tr;
        $('#moneyLogs tr:lt('+x+')').show();
        if(x >= size_tr){
            show_more.hide();
        }
    });
    /* end view and show showmore button script */

    /* view and show elastic progress */
    var $elem = $('.pricebar');
    $.each($elem, function( index, value ) {
        var ep = new ElasticProgress(value, {
            fontFamily: "Roboto",
            colorFg: "#77c2ff",
            colorBg: "#4e80dd",
            highlightColor: "#ed7499",
            barHeight: 14,
            barInset: 10,
            textFail: $(value).data('collected-full') + " grn",
            width: $('.goal-view-price').width() - 15
        });

        ep.open();

        ep.onOpen(function () {
            fakeLoading(ep, 2, $(value).data('collected-partial'));
        });
    });


    /* view and show end elastic progress */

    $('#create_goal_form').on('submit', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var data = new FormData();
        $.each(dz.getQueuedFiles(), function(i, file) {
            data.append('files[]', file);
        });

        var form = $(this).serializeArray();
        $.each(form, function (i, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                console.log(data.status);
                if(data.status == 'success') {
                    location.href = data.url;
                }
            }
        });
    });
});
