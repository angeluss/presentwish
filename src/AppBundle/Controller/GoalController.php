<?php

namespace AppBundle\Controller;

use ApiBundle\Entity\Goal;
use ApiBundle\Entity\GoalImage;
use ApiBundle\Entity\MoneyLog;
use ApiBundle\Entity\Wish;
use AppBundle\Form\Type\Goal\CreateGoalFormType;
use AppBundle\Form\Type\Goal\EditGoalFormType;
use AppBundle\Form\Type\MoneyLog\MoneyLogFormType;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Services\Liqpay\LiqPay;
use Symfony\Component\HttpFoundation\Response;

class GoalController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $goals = $em->getRepository('ApiBundle:Goal')->getAllForUser($this->getUser()->getId());
        return $this->render('AppBundle:Goal:index.html.twig', array(
            'goals' => $goals,
        ));
    }

    public function createAction(Request $request, Wish $wish)
    {
        $em = $this->getDoctrine()->getManager();
        $pr = $this->container->get('sonata.media.provider.image');
        $goal = Goal::createFromWish($wish, $em);

        if($this->getUser() != $wish->getUser())
            return $this->redirect($this->generateUrl('user_403'));

        $targets = $this->getDoctrine()
            ->getRepository('ApiBundle:GoalTarget')
            ->getAllPublic();
        $options = array(
            'targets' => $targets,
        );

        $form = $this->createForm(new CreateGoalFormType($options), $goal);
        $form->handleRequest($request);

        if($form->isValid()) {
            $ids = $request->request->get('media_id');

            $goal->setUser($this->getUser());
            $goal->setCreatedAt(new \DateTime('now'));
            $goal->setModifiedAt(new \DateTime('now'));
            $goal->setImagesFromWish($wish, $ids);

            $goal->setDateFinish(new \DateTime($request->request->get('date')));

            if(!is_null($request->files->get('files'))) {
                foreach ($request->files->get('files') as $file) {

                    $media = $this->createMedia($file);

                    $image = new GoalImage();
                    $image->setMedia($media);
                    $image->setCreatedAt(new \DateTime('now'));
                    $image->setGoal($goal);

                    $goal->addImage($image);
                }
            }

            $em->persist($goal);
            $em->flush();

            return new JsonResponse(array(
                'status'=>'success',
                'url' => $this->generateUrl('goals'),
            ));
        }
        $images = array();
        foreach($wish->getImages() as $img){
            if(!is_null($img->getMedia())) {
                $images[] = [
                    'name' => $img->getMedia()->getName(),
                    'size' => $img->getMedia()->getSize(),
                    'thumb' => $pr->generatePublicUrl($img->getMedia(), $pr->getFormatName($img->getMedia(), 'small')),
                    'id' => $img->getMedia()->getId(),
                ];
            } else {
                $images[] = [
                    'name' => 'unknown',
                    'size' => 'unknown',
                    'thumb' => $img->getUrl(),
                ];
            }
        }
        return $this->render('AppBundle:Goal:create.html.twig', array(
            'form' => $form->createView(),
            'wish' => $wish,
            'goal' => $goal,
            'images' => $images,
        ));
    }

    /**
     * @param $file UploadedFile
     * @return Media
     */
    protected function createMedia($file){
        $fileName = md5(uniqid()) . '_' . $file->getClientOriginalName();
        $image_info = getimagesize($file->getRealPath());
        $width = $image_info[0];
        $height = $image_info[1];

        $media = new Media();
        $media->setName($file->getClientOriginalName());
        $media->setEnabled(false);
        $media->setProviderName('sonata.media.provider.image');
        $media->setProviderReference($fileName);
        $media->setProviderMetadata(['filename' => $file->getClientOriginalName()]);
        $media->setWidth($width);
        $media->setHeight($height);
        $media->setContext('default');
        $media->setBinaryContent($file);
        $media->setContentType($file->getClientMimeType());
        $media->setSize($file->getClientSize());
        $media->setCreatedAt(new \DateTime('now'));

        return $media;
    }

    public function viewAction($id)
    {
        $goal = $this->getDoctrine()
            ->getRepository('ApiBundle:Goal')
            ->find($id);

        if (!$goal) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        if(isset($_SERVER['HTTP_REFERER'])){
            $goal->setStatistics($_SERVER['HTTP_REFERER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($goal);
            $em->flush();
        }

        return $this->render('AppBundle:Goal:view.html.twig', array(
            'goal' => $goal,
        ));
    }

    /**
     * Template("AppBundle:Goal:edit.html.twig")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pr = $this->container->get('sonata.media.provider.image');
        $goal = $this->getDoctrine()
            ->getRepository('ApiBundle:Goal')
            ->find($id);
        $targets = $this->getDoctrine()
            ->getRepository('ApiBundle:GoalTarget')
            ->getAllPublic();
        $options = array(
            'targets' => $targets,
        );

        if($this->getUser() != $goal->getUser())
            return $this->redirect($this->generateUrl('user_403'));

        $form = $this->createForm(new EditGoalFormType($options), $goal);
        $form->handleRequest($request);

        if($form->isValid()) {
            $ids = $request->request->get('media_id');

            if(!is_null($ids)) {
                foreach ($ids as $media_id) {
                    $image = $this->getDoctrine()
                        ->getRepository('ApiBundle:GoalImage')
                        ->getAllForMedia($media_id);

                    if (isset($image[0])) {
                        $goal->removeImage($image[0]);
                    }
                }
            }

            $goal->setModifiedAt(new \DateTime('now'));
            if(!is_null($request->files->get('files'))) {
                foreach ($request->files->get('files') as $file) {

                    $media = $this->createMedia($file);

                    $image = new GoalImage();
                    $image->setMedia($media);
                    $image->setCreatedAt(new \DateTime('now'));
                    $image->setGoal($goal);

                    $goal->addImage($image);
                }
            }

            $em->persist($goal);
            $em->flush();
            return new JsonResponse(array(
                'status'=>'success',
                'url' => $this->generateUrl('goals'),
            ));

//            return $this->redirect($this->generateUrl('goal_view', array('id' => $id)));
        }
        $images = array();
        foreach($goal->getImages() as $img){
            if(!is_null($img->getMedia())) {
                $images[] = [
                    'name' => $img->getMedia()->getName(),
                    'size' => $img->getMedia()->getSize(),
                    'thumb' => $pr->generatePublicUrl($img->getMedia(), $pr->getFormatName($img->getMedia(), 'small')),
                    'id' => $img->getMedia()->getId(),
                ];
            } else {
                $images[] = [
                    'name' => 'unknown',
                    'size' => 'unknown',
                    'thumb' => $img->getUrl(),
                ];
            }
        }
//        dump($images); die;
        return $this->render('AppBundle:Goal:edit.html.twig', array(
            'form' => $form->createView(),
            'goal' => $goal,
            'images' => $images,
        ));
    }
    public function deleteAction(Request $request, Goal $goal){
        $em = $this->getDoctrine()->getManager();

        # If user isn't owner -> 403
        if($this->getUser() != $goal->getUser())
            return $this->redirect($this->generateUrl('user_403'));

        $session = $request->getSession();

        try {
            $em->remove($goal);
            $em->flush();
            $session->getFlashBag()->add('msgSuccess', $this->get('translator')->trans('delete_success'));
        } catch(\Exception $e) {
            $session->getFlashBag()->add('msgError', $this->get('translator')->trans('goal.cannot_delete'));
        }

        return $this->redirect($this->generateUrl('goals'));
    }

    public function deleteImageAction(){
        $id = $this->container->get('request')->request->get('id');

        $em = $this->getDoctrine()->getManager();

        $image = $this->getDoctrine()
            ->getRepository('ApiBundle:GoalImage')
            ->find($id);

        if($this->getUser() != $image->getGoal()->getUser())
            return new JsonResponse(array(
                'success' => false,
            ));

        try {
            $em->remove($image);
            $em->flush();
        } catch(\Exception $e) {
            return new JsonResponse(array(
                'success' => false,
            ));
        }

        return new JsonResponse(array(
            'success' => true,
        ));

    }

    public function showAction(Request $request, $id)
    {
        $goal = $this->getDoctrine()
            ->getRepository('ApiBundle:Goal')
            ->find($id);

        if (!$goal) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        if(isset($_SERVER['HTTP_REFERER'])){
            $goal->setStatistics($_SERVER['HTTP_REFERER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($goal);
            $em->flush();
        }

        return $this->render('AppBundle:Goal:show.html.twig', array(
            'goal' => $goal,
        ));
    }

    public function payAction(Request $request, $id){
        $json_string = array(
            'action'         => 'pay',
            'amount'         => '1',
            'currency'       => LiqPay::CURRENCY_UAH,
            'description'    => 'description text',
            'order_id'       => time(),
            'sandbox'        => $this->container->getParameter('liqpay_sandbox'),
            'version'        => 3,
            'server_url'     => $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->generateUrl('pay_get'),
            'result_url'     => $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->generateUrl('pay_show'),
        );
        dump($json_string);
        $link = $this->generateUrl('pay_show');
        echo '<a href="' . $link . '" />link</a>';
        $liqpay = $this->get('api.liqpay');

        $html = $liqpay->cnb_form($json_string);
        echo $html; die;

        $em = $this->getDoctrine()->getManager();
        $goal = $this->getDoctrine()
            ->getRepository('ApiBundle:Goal')
            ->find($id);

        if (!$goal) {
            throw $this->createNotFoundException(
                'No product found for id '. $id
            );
        }

        $moneyLog = new MoneyLog();

        $form = $this->createForm(new MoneyLogFormType(), $moneyLog);
        $form->handleRequest($request);

        if($form->isValid()) {
            $securityContext = $this->container->get('security.authorization_checker');
            if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $moneyLog->setUser($this->getUser());
            }

            $moneyLog->setCreatedAt(new \DateTime('now'));
            $moneyLog->setGoal($goal);
            $moneyLog->setStatus(MoneyLog::STATUS_SUCCESS);

            $em->persist($goal);
            $em->flush();

            return $this->redirect($this->generateUrl('goal_view', array('id' => $goal->getId())));
        }

        return $this->render('AppBundle:Goal:create.html.twig', array(
            'form' => $form->createView(),
            'goal' => $goal,
        ));
    }

    public function manualAction(Request $request, $id){

//        $em = $this->getDoctrine()->getManager();
//        $goal = $this->getDoctrine()
//            ->getRepository('ApiBundle:Goal')
//            ->find($id);
//
//        if (!$goal) {
//            throw $this->createNotFoundException(
//                'No product found for id '. $id
//            );
//        }
//
//        $moneyLog = new MoneyLog();
//
//        $form = $this->createForm(new MoneyLogFormType(), $moneyLog);
//        $form->handleRequest($request);
//
//        if($form->isValid()) {
//            $securityContext = $this->container->get('security.authorization_checker');
//            if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//                $moneyLog->setUser($this->getUser());
//            }
//
//            $moneyLog->setCreatedAt(new \DateTime('now'));
//            $moneyLog->setGoal($goal);
//            $moneyLog->setStatus(MoneyLog::STATUS_SUCCESS);
//
//            $em->persist($goal);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('goal_view', array('id' => $goal->getId())));
//        }

        return $this->render('AppBundle:Goal:manual.html.twig', array(
//            'form' => $form->createView(),
//            'goal' => $goal,
        ));
    }

    public function showpayAction(){
        if (count($_POST)) {
            file_put_contents(dirname(__FILE__).'../../../web/uploads/showpay_robot.php', var_export($_POST, true) . "\r\n\r\n", FILE_APPEND | LOCK_EX);
            dump($_POST);
        }
        return $this->render('AppBundle:Goal:showsuccesspay.html.twig', array(
        ));
    }

    public function getpayAction(){
        if (count($_POST)) {
            file_put_contents(dirname(__FILE__).'../../../web/uploads/getpay_robot.php', var_export($_POST, true) . "\r\n\r\n", FILE_APPEND | LOCK_EX);
        }
        return new Response();
    }
}
