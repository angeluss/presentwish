<?php

namespace AppBundle\Controller;


use ApiBundle\Entity\User;
use ApiBundle\Entity\UserImage;
use FOS\UserBundle\Model\UserManager;
use HWI\Bundle\OAuthBundle\Controller\ConnectController;
use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use Symfony\Component\HttpFoundation\Request;

class OauthController extends ConnectController
{
    public function registrationAction(Request $request, $key)
    {
        $connect = $this->container->getParameter('hwi_oauth.connect');
        $userManager = $this->container->get('fos_user.user_manager');
        if (!$connect) {
            throw new NotFoundHttpException();
        }

        $hasUser = $this->isGranted('IS_AUTHENTICATED_REMEMBERED');
        if ($hasUser) {
            throw new AccessDeniedException('Cannot connect already registered account.');
        }

        $session = $request->getSession();
        $error = $session->get('_hwi_oauth.registration_error.'.$key);
        $session->remove('_hwi_oauth.registration_error.'.$key);

        if (!$error instanceof AccountNotLinkedException || time() - $key > 300) {
            throw new \Exception('Cannot register an account.');
        }

        $userInformation = $this
            ->getResourceOwnerByName($error->getResourceOwnerName())
            ->getUserInformation($error->getRawToken())
        ;

        $resourse = $userInformation->getResourceOwner()->getName();
        switch ($resourse){
            case 'vkontakte':
                $user = $this->authVK($request, $error, $userInformation, $userManager);
                break;
            case 'facebook':
                $user = $this->authFB($request, $error, $userInformation, $userManager);
                break;
            case 'google':
                $user = $this->authGoogle($request, $error, $userInformation, $userManager);
                break;
            default:
                throw new \Exception('Unknown service.');

        }
        // Authenticate the user
        $this->authenticateUser($request, $user, $error->getResourceOwnerName(), $error->getRawToken());

        return $this->redirect($this->generateUrl('wishes'));

    }

    public function authVK($request, $error, $userInformation, UserManager $userManager){
        $email = $userInformation->getEmail();
        $user = $userManager->findUserByEmail($email);

        if(is_null($user)){
            $user = $userManager->createUser();
            $username = $userInformation->getRealname();
            if (is_null($username)){
                $username = $userInformation->getFirstName() . ' ' . $userInformation->getLastName();
            }
            $imgUrl = $userInformation->getProfilePicture();
            if(is_null($imgUrl)){
                if(isset($userInformation->getResponse()['response'][0]['photo_medium'])){
                    $imgUrl = $userInformation->getResponse()['response'][0]['photo_medium'];
                }
            }
            $image = $this->createUserImage($imgUrl, $user);

            $user->setEmail($email);
            $user->setUsername($username);
            $user->setEmailCanonical(strtolower($email));
            $user->setUsernameCanonical(strtolower($username));
            $user->setEnabled(1);
            $user->setImage($image);
            $user->setVkontakteId($userInformation->getUsername());

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();


        } else {
            $user->setVkontakteId($userInformation->getUsername());
            $userManager->updateUser($user);
        }
        return $user;
    }

    public function authFB($request, $error, $userInformation, UserManager $userManager){

        $email = $userInformation->getEmail();
        $user = $userManager->findUserByEmail($email);

        if(is_null($user)){
            $user = $userManager->createUser();
            $username = $userInformation->getRealname();
            if (is_null($username)){
                $username = $userInformation->getFirstName() . ' ' . $userInformation->getLastName();
            }
            $imgUrl = $userInformation->getProfilePicture();
            $image = $this->createUserImage($imgUrl, $user);

            $user->setEmail($email);
            $user->setUsername($username);
            $user->setEmailCanonical(strtolower($email));
            $user->setUsernameCanonical(strtolower($username));
            $user->setEnabled(1);
            $user->setImage($image);
            $user->setFacebookId($userInformation->getUsername());

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();


        } else {
            $user->setFacebookId($userInformation->getUsername());
            $userManager->updateUser($user);
        }

        return $user;
    }

    public function authGoogle($request, $error, $userInformation, UserManager $userManager){

        if(true) {
            dump($userInformation); die;
        }

        $email = $userInformation->getEmail();
        $user = $userManager->findUserByEmail($email);

        if(is_null($user)){
            $user = $userManager->createUser();
            $username = $userInformation->getRealname();
            if (is_null($username)){
                $username = $userInformation->getFirstName() . ' ' . $userInformation->getLastName();
            }
            $imgUrl = $userInformation->getProfilePicture();
            $image = $this->createUserImage($imgUrl, $user);

            $user->setEmail($email);
            $user->setUsername($username);
            $user->setEmailCanonical(strtolower($email));
            $user->setUsernameCanonical(strtolower($username));
            $user->setEnabled(1);
            $user->setImage($image);
            $user->setGoogleId($userInformation->getUsername());

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
        } else {
            $user->setGoogleId($userInformation->getUsername());
            $userManager->updateUser($user);
        }
        return $user;
    }

    public function createUserImage($url, $user){
        $image = new UserImage();
        $image->setChangedAt(new \DateTime('now'));
        $image->setUrl($url);
        $image->setGetFrom(1);
        $image->setType(1);
        $image->setUser($user);
        return $image;
    }
}