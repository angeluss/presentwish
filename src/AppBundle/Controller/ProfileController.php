<?php

namespace AppBundle\Controller;

use \FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;


class ProfileController extends BaseController
{
    /**
     * Show the user
     */
    public function showAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        // Gets the amount of money logs
        $moneyLogs=$user->getMoneyLogs();
        $amountSum = 0;
        foreach($moneyLogs as $item){
            $amountSum += $item->getAmount();
        }

        return $this->container->get('templating')->renderResponse('AppBundle:Profile:show.html.'.$this->container->getParameter('fos_user.template.engine'), array('user' => $user,'amountSum' => $amountSum));
    }

    /**
     * Edit the user
     */
    public function editAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->container->get('fos_user.profile.form');
        $formHandler = $this->container->get('fos_user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->getRedirectionUrl($user));
        }

        $formPass = $this->container->get('fos_user.change_password.form');
        $formPassHandler = $this->container->get('fos_user.change_password.form.handler');

        $process = $formPassHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->getRedirectionUrl($user));
        }

        return $this->container->get('templating')->renderResponse('AppBundle:Profile:edit.html.'.$this->container->getParameter('fos_user.template.engine'), array(
                'form' => $form->createView(),
                'user' => $user,
                'formPasswords' => $formPass->createView()
            )
        );
    }
}