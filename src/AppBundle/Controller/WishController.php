<?php

namespace AppBundle\Controller;

use ApiBundle\Entity\Wish;
use ApiBundle\Entity\WishImage;
use ApiBundle\Services\Parser\ApiParserService;
use AppBundle\Form\Type\Wish\EditWishFormType;
use AppBundle\Form\Type\Wish\CreateWishFormType;
use AppBundle\Form\Type\Wish\WishImagesType;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WishController extends Controller
{

    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Wish');
        $wishes = $repository->getAllForUser($this->getUser()->getId());
        $pr = $this->container->get('sonata.media.provider.image');

        $result_wishes = array();
        foreach ($wishes as $wish) {
            $images = $wish->getImages()->getValues();
            if(!empty($images)) {
                if (!is_null($wish->getImages()[0]->getMedia())) {
                    $img = $pr->generatePublicUrl($wish->getImages()[0]->getMedia(), $pr->getFormatName($wish->getImages()[0]->getMedia(), 'big'));
                } else {
                    $img = $wish->getImages()[0]->getUrl();
                }
            } else {
                $img = '/bundles/app/img/noimage.jpg';
            }

            $result_wishes[] = array(
                'id' => $wish->getId(),
                'img' => $img,
                'title' => $wish->getTitle(),
                'price' => '₴' . $wish->getPrice(),
            );
        }
        return $this->render('AppBundle:Wish:index.html.twig', array(
            'wishes' => $wishes,
            'result_wishes' => $result_wishes,
        ));
    }

    /**
     * @param $id
     * @return Response
     * Template("AppBundle:Wish:view.html.twig")
     */
    public function viewAction($id)
    {
        $wish = $this->getDoctrine()
            ->getRepository('ApiBundle:Wish')
            ->find($id);

        if (!$wish) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        return $this->render('AppBundle:Wish:view.html.twig', array(
            'wish' => $wish,
        ));
    }

    /**
     * @param $id
     * @return Response
     * Template("AppBundle:Wish:show.html.twig")
     */
    public function showAction($id)
    {
        $wish = $this->getDoctrine()
            ->getRepository('ApiBundle:Wish')
            ->find($id);

        if (!$wish) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        return $this->render('AppBundle:Wish:show.html.twig', array(
            'wish' => $wish,
        ));
    }

    /**
     * Template("AppBundle:Wish:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $wish = new Wish();

        $form = $this->createForm(new CreateWishFormType, $wish);
        $form->handleRequest($request);

        if($form->isValid()) {

            $wish->setUser($this->getUser());
            $wish->setCreatedAt(new \DateTime('now'));
            $wish->setModifiedAt(new \DateTime('now'));

            if($media_id = $request->request->get('media_id')){
                $media = $this->get('sonata.media.manager.media')->find($media_id);
                $image = new WishImage();
                $image->setMedia($media);
                $image->setCreatedAt(new \DateTime('now'));
                $image->setWish($wish);

                $wish->addImage($image);
            }
            if(!is_null($request->files->get('files'))) {
                foreach($request->files->get('files') as $file){

                    $media = $this->createMedia($file);

                    $image = new WishImage();
                    $image->setMedia($media);
                    $image->setCreatedAt(new \DateTime('now'));
                    $image->setWish($wish);

                    $wish->addImage($image);
                }
            }
            $em->persist($wish);
            $em->flush();

//            return new Response(json_encode(array(
//                'status'=>'success',
//                'url' => $this->generateUrl('wishes')
//                )));
            return new JsonResponse(array(
                'status'=>'success',
                'url' => $this->generateUrl('wishes'),
            ));

//            return $this->redirect($this->generateUrl('wishes'));
        }

        return $this->render('AppBundle:Wish:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Template("AppBundle:Wish:create.html.twig")
     */
    public function copyAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $pr = $this->container->get('sonata.media.provider.image');
        $old_wish = $this->getDoctrine()
            ->getRepository('ApiBundle:Wish')
            ->find($id);
        $wish = clone $old_wish;
        $wish->setId(null);

        $form = $this->createForm(new EditWishFormType(), $wish);
        $form->handleRequest($request);

        if($form->isValid()) {
            $ids = $request->request->get('media_id');

            if(!is_null($ids)) {
                foreach ($ids as $media_id) {
                    $image = $this->getDoctrine()
                        ->getRepository('ApiBundle:WishImage')
                        ->getAllForMedia($media_id);
                    if (isset($image[0])) {
                        $wish->removeImage($image[0]);
                    }
                }
            }
            $wish->setCreatedAt(new \DateTime('now'));
            $wish->setModifiedAt(new \DateTime('now'));
            foreach ($old_wish->getImages() as $img){
                if(is_null($ids) || !in_array($img->getId(), $ids) ){
                    $wish->addImage($img);
                }
            }
            if(!is_null($request->files->get('files'))) {
                foreach ($request->files->get('files') as $file) {

                    $media = $this->createMedia($file);

                    $image = new WishImage();
                    $image->setMedia($media);
                    $image->setCreatedAt(new \DateTime('now'));
                    $image->setWish($wish);

                    $wish->addImage($image);
                }
            }
            $em->persist($wish);
            $em->flush();

            return new JsonResponse(array(
                'status'=>'success',
                'url' => $this->generateUrl('wishes'),
            ));

        }
        $images = array();
        foreach($wish->getImages() as $img){
            if(!is_null($img->getMedia())) {
                $images[] = [
                    'name' => $img->getMedia()->getName(),
                    'size' => $img->getMedia()->getSize(),
                    'thumb' => $pr->generatePublicUrl($img->getMedia(), $pr->getFormatName($img->getMedia(), 'small')),
                    'id' => $img->getMedia()->getId(),
                ];
            } else {
                $images[] = [
                    'name' => 'unknown',
                    'size' => 'unknown',
                    'thumb' => $img->getUrl(),
                ];
            }
        }

        return $this->render('AppBundle:Wish:edit.html.twig', array(
            'form' => $form->createView(),
            'wish' => $wish,
            'images' => $images,
        ));
    }

    /**
     * Template("AppBundle:Wish:edit.html.twig")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pr = $this->container->get('sonata.media.provider.image');
        $wish = $this->getDoctrine()
            ->getRepository('ApiBundle:Wish')
            ->find($id);

        if($this->getUser() != $wish->getUser())
            return $this->redirect($this->generateUrl('user_403'));

        $form = $this->createForm(new EditWishFormType(), $wish);
        $form->handleRequest($request);

        if($form->isValid()) {
            $ids = $request->request->get('media_id');
            if(!is_null($ids)) {
                foreach ($ids as $media_id) {
                    $image = $this->getDoctrine()
                        ->getRepository('ApiBundle:WishImage')
                        ->getAllForMedia($media_id);
                    if (isset($image[0])) {
                        $wish->removeImage($image[0]);
                    }
                }
            }
            $wish->setModifiedAt(new \DateTime('now'));
            if(!is_null($request->files->get('files'))) {
                foreach ($request->files->get('files') as $file) {

                    $media = $this->createMedia($file);

                    $image = new WishImage();
                    $image->setMedia($media);
                    $image->setCreatedAt(new \DateTime('now'));
                    $image->setWish($wish);

                    $wish->addImage($image);
                }
            }
            $em->persist($wish);
            $em->flush();

            return new JsonResponse(array(
                'status'=>'success',
                'url' => $this->generateUrl('wishes'),
            ));

//            return $this->redirect($this->generateUrl('wish_view', array('id' => $id)));
        }
        $images = array();
        foreach($wish->getImages() as $img){
            if(!is_null($img->getMedia())) {
                $images[] = [
                    'name' => $img->getMedia()->getName(),
                    'size' => $img->getMedia()->getSize(),
                    'thumb' => $pr->generatePublicUrl($img->getMedia(), $pr->getFormatName($img->getMedia(), 'small')),
                    'id' => $img->getMedia()->getId(),
                ];
            } else {
                $images[] = [
                    'name' => 'unknown',
                    'size' => 'unknown',
                    'thumb' => $img->getUrl(),
                ];
            }
        }

        return $this->render('AppBundle:Wish:edit.html.twig', array(
            'form' => $form->createView(),
            'wish' => $wish,
            'images' => $images,
        ));
    }

    /**
     * @param $file UploadedFile
     * @return Media
     */
    protected function createMedia($file){
        $fileName = md5(uniqid()) . '_' . $file->getClientOriginalName();
        $image_info = getimagesize($file->getRealPath());
        $width = $image_info[0];
        $height = $image_info[1];

        $media = new Media();
        $media->setName($file->getClientOriginalName());
        $media->setEnabled(false);
        $media->setProviderName('sonata.media.provider.image');
        $media->setProviderReference($fileName);
        $media->setProviderMetadata(['filename' => $file->getClientOriginalName()]);
        $media->setWidth($width);
        $media->setHeight($height);
        $media->setContext('default');
        $media->setBinaryContent($file);
        $media->setContentType($file->getClientMimeType());
        $media->setSize($file->getClientSize());
        $media->setCreatedAt(new \DateTime('now'));

        return $media;
    }

    public function getWishDataAction(){
        $url = $this->container->get('request')->request->get('link');

        $parser = new ApiParserService($url);
        $data =  $parser->parse();
        $file = $parser->putImg();
        $media_id = false;
        $preview = array();

        if((false !== $file) && ($media = $this->createMedia($file))) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($media);

            $em->flush();
            $media_id = $media->getId();
            $pr = $this->container->get('sonata.media.provider.image');
            $preview = [
                'name' => $media->getName(),
                'size' => $media->getSize(),
                'thumb' => $pr->generatePublicUrl($media, $pr->getFormatName($media, 'small')),
            ];
        }
        if($data) {
            return new JsonResponse(array(
                'success' => true,
                'title' => $data['title'],
                'description' => $data['description'],
                'price' => $data['price'],
                'image' => $data['image'],
                'media' => $media_id,
                'preview' => $preview,
            ));
        } else {
            return new JsonResponse(array(
                'success' => false,
            ));
        }
    }

    public function getCreateWishFormAction(Request $request)
    {
        $form = $this->getCreateWishForm();
        return array(
            'form' => $form->createView(),
        );
    }

    protected function getCreateWishForm()
    {
        return $this->createForm(CreateWishFormType::class);
    }

    public function deleteImageAction(){
        $id = $this->container->get('request')->request->get('id');

        $em = $this->getDoctrine()->getManager();

        $image = $this->getDoctrine()
            ->getRepository('ApiBundle:WishImage')
            ->find($id);

        if($this->getUser() != $image->getWish()->getUser())
            return new JsonResponse(array(
                'success' => false,
            ));

        try {
            $em->remove($image);
            $em->flush();
        } catch(\Exception $e) {
            return new JsonResponse(array(
                'success' => false,
            ));
        }

        return new JsonResponse(array(
            'success' => true,
        ));

    }

    public function deleteAction(Request $request, Wish $wish){
        $em = $this->getDoctrine()->getManager();
//        $wish = $this->getDoctrine()
//            ->getRepository('ApiBundle:Wish')
//            ->find($id);

        # If user isn't owner -> 403
        if($this->getUser() != $wish->getUser())
            return $this->redirect($this->generateUrl('user_403'));

        $session = $request->getSession();

        try {
            $em->remove($wish);
            $em->flush();
            $session->getFlashBag()->add('msgSuccess', $this->get('translator')->trans('delete_success'));
        } catch(\Exception $e) {
            $session->getFlashBag()->add('msgError', $this->get('translator')->trans('wish.cannot_delete'));
        }

        return $this->redirect($this->generateUrl('wishes'));
    }
}

