<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:MainPage:index.html.twig', array(
            'test' => 'test'
        ));
    }

    /**
     * 403 Redirect
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function noAccessAction() {
        return $this->render('AppBundle::403.html.twig');
    }
}
