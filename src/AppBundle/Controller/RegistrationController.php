<?php

namespace AppBundle\Controller;

use ApiBundle\Entity\User;
use AppBundle\Form\Type\Registration\SignUpFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\Registration\RegistrationFormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

class RegistrationController extends Controller
{

    public function validateAndCreateUserAction(Request $request)
    {
        $form = $this->getRegistreationForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();
            $existentUser = $this->checkUserExistense($formData['email']);
            if ($existentUser) {
                $translator = $this->get('translator');
                $form->get('email')->addError(new FormError($translator->trans('registration.email.error', array(), 'app')));
                $errors = $this->getFormErrors($form);
                return new JsonResponse(array(
                    'success' => false,
                    'errors'=>$errors
                ));
            }
            $this->get('api.user_service')->createUser($formData);
            return new JsonResponse(array('success' => true));
        } else {
            $errors = $this->getFormErrors($form);
            return new JsonResponse(array(
                'success' => false,
                'errors'=>$errors
            ));
        }
    }

    protected function getFormErrors($form)
    {
        $forms = $form->all();
        $errors = array();
        foreach($forms as $singleForm){
            foreach ($singleForm->getErrors(true, true) as $error) {
                $errors[] = array(
                    'field' => $singleForm->getName(),
                    'message' => $error->getMessageTemplate(),
                );
            }
        }

        return $errors;
    }

    protected function checkUserExistense($email)
    {
        $user = $this->getDoctrine()->getManager()->getRepository('ApiBundle:User')->findOneByEmail($email);
        if ($user) {
            return true;
        }

        return false;
    }
    /**
     * @Template("AppBundle:Registration:registration.html.twig")
     */
    public function getRegistrationFormAction(Request $request)
    {
        $form = $this->getRegistreationForm();
        return array(
            'form' => $form->createView(),
        );
    }

    protected function getRegistreationForm()
    {
        return $this->createForm(RegistrationFormType::class);
    }

    /**
     * @Template("AppBundle:Registration:login.html.twig")
     */
    public function getSignUpFormAction()
    {
        $form = $this->getSignupForm();
        return array(
            'form' => $form->createView(),
        );
    }

    protected function getSignupForm()
    {
        return $this->createForm(SignUpFormType::class);
    }

    public function signUpUserAction(Request $request)
    {
        $form = $this->getSignupForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $success = $this->get('api.user_service')->authenticateUser($form->getData());
            if ($success) {
                return new JsonResponse(array('success' => true));
            }
        }
        $translator = $this->get('translator');
        $message = $translator->trans('registration.user_not_found', array(), 'app');

        return new JsonResponse(array(
            'success' => false,
            'message' => $message,
        ));
    }
}
