<?php
/**
 * Created by PhpStorm.
 * User: angeluss
 * Date: 9/1/16
 * Time: 3:53 PM
 */

namespace AppBundle\Controller;

use ApiBundle\Entity\AmountLogs;
use AppBundle\Form\Type\WithdrawFundsFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use AppBundle\Form\Type\AddFundsFormType;

class BalanceController extends Controller
{
    public function addFundsAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $log = new AmountLogs();
        $form = $this->createForm(new AddFundsFormType(), $log);
        $form->handleRequest($request);
        if($form->isValid()) {
            $log->setUser($this->getUser());
            $log->setStatus(1);
            $log->setDate(new \DateTime('now'));
            $log->setType(AmountLogs::TYPE_IN);

            $data = [
                'user' => $user,
                'amount' => $log->getAmount(),
            ];
            if($this->get('api.user_service')->addFunds($data)) {
                $em->persist($log);
                $em->flush();
            } else {
                throw new Exception('Can\'t add funds');
            }
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        return $this->render('AppBundle:Balance:add_funds.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    public function withdrawFundsAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $log = new AmountLogs();
        $form = $this->createForm(new WithdrawFundsFormType(), $log);
        $form->handleRequest($request);
        if($form->isValid()) {
            $log->setUser($this->getUser());
            $log->setStatus(1);
            $log->setDate(new \DateTime('now'));
            $log->setType(AmountLogs::TYPE_OUT);

            $data = [
                'user' => $user,
                'amount' => $log->getAmount(),
            ];

            $result = $this->get('api.user_service')->withdrawFunds($data);
            if(false !== $result['success']) {
                $em->persist($log);
                $em->flush();
            } else {
                throw new Exception($result['error']);
            }

            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render('AppBundle:Balance:add_funds.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    public function showAllAction(){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $logs = $em->getRepository('ApiBundle:AmountLogs')->getAllForUser($user->getId());
        return $this->render('AppBundle:Balance:show_all.html.twig', array(
            'logs' => $logs,
            'user' => $user,
        ));
    }
}