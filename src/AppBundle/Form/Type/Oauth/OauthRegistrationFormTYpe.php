<?php
/**
 * Created by PhpStorm.
 * User: sandro
 * Date: 8/19/16
 * Time: 6:05 PM
 */

namespace AppBundle\Form\Type\Oauth;

use AppBundle\Form\Type\ProfileImagesType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OauthRegistrationFormTYpe extends RegistrationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('image', ProfileImagesType::class, array(
    ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
        ));
    }
}