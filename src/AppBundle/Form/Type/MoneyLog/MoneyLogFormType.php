<?php
/**
 * Created by PhpStorm.
 * User: sandro
 * Date: 9/2/16
 * Time: 3:50 PM
 */

namespace AppBundle\Form\Type\MoneyLog;

use ApiBundle\Entity\MoneyLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MoneyLogFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'moneylog.username',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'username',
                ),
                'required' => false,
            ))
            ->add('amount', IntegerType::class, array(
                'label' => 'moneylog.title',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Title',
                ),
                'required' => true,
            ))
            ->add('access', ChoiceType::class, array(
                'choices' => array(
                    MoneyLog::PUBLIC_ACCESS => 'moneylog.public' ,
                    MoneyLog::PRIVATE_ACCESS => 'moneylog.private',
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data'     => MoneyLog::PUBLIC_ACCESS,
            ))
            ->add('pPay', SubmitType::class, array(
                'label' => 'moneylog.pay',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
            'data_class' => 'ApiBundle\Entity\MoneyLog',
        ));
    }
}