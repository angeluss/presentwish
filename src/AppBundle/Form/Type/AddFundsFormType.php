<?php
/**
 * Created by PhpStorm.
 * User: sandro
 * Date: 9/1/16
 * Time: 12:40 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ApiBundle\Entity\AmountLogs;

class AddFundsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('service', ChoiceType::class, array(
                'choices' => array(
                    AmountLogs::SERVICE_LIQPAY => 'LiqPay' ,
                ),
                'attr' => array(
                    'class' => 'pay_item',
                ),
//                'label' => false,
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data'     => AmountLogs::SERVICE_LIQPAY,
            ))
            ->add('amount', IntegerType::class, array(
                'label' => false,
                'attr' => array(
                    'class' => 'text_block',
                ),
                'required' => false,
            ))
            ->add('create', SubmitType::class, array(
                'label' => 'add_funds',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\AmountLogs',
        ));
    }
}