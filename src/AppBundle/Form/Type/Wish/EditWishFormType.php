<?php

namespace AppBundle\Form\Type\Wish;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class EditWishFormType extends CreateWishFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('create');
        $builder->add('edit', SubmitType::class, array(
            'label' => 'wish_edit.submit',
            'attr' => array(
                'class' => 'btn btn-primary'
            ),
        ));
        $builder->remove('access');
        $builder->add('access', ChoiceType::class, array(
            'choices' => array(
                self::PUBLIC_ACCESS => 'wish.public' ,
                self::PROTECTED_ACCESS => 'wish.protected',
                self::PRIVATE_ACCESS => 'wish.private',
            ),
            'multiple' => false,
            'expanded' => true,
            'required' => true,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
        ));
    }
}