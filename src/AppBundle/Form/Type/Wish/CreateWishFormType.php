<?php

namespace AppBundle\Form\Type\Wish;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CreateWishFormType extends AbstractType
{
    const PUBLIC_ACCESS = 1;
    const PROTECTED_ACCESS = 2;
    const PRIVATE_ACCESS = 3;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', TextType::class, array(
                'label' => 'wish.url',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'URL',
                ),
                'required' => true,
            ))
            ->add('title', TextType::class, array(
                'label' => 'wish.title',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Title',
                ),
                'required' => true,
            ))
            ->add('images', CollectionType::class, array(
                'entry_type' => WishImagesType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            ))
            ->add('price', IntegerType::class, array(
                'label' => 'wish.price',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Price',
                ),
                'required' => true,
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'wish.description',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'wish-description-editor',
                ),
                'required' => true,
            ))
            ->add('access', ChoiceType::class, array(
                'choices' => array(
                    self::PUBLIC_ACCESS => 'wish.public' ,
                    self::PROTECTED_ACCESS => 'wish.protected',
                    self::PRIVATE_ACCESS => 'wish.private',
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data'     => self::PUBLIC_ACCESS,
            ))
            ->add('create', SubmitType::class, array(
                'label' => 'wish.create',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
            'data_class' => 'ApiBundle\Entity\Wish',
        ));
    }
}