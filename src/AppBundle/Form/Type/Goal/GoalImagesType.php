<?php

namespace AppBundle\Form\Type\Goal;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GoalImagesType extends AbstractType
{
    const URL_TYPE = 1;
    const FILE_TYPE = 2;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    self::URL_TYPE => 'goalImage.via_url' ,
                    self::FILE_TYPE => 'goalImage.via_fileInput',
                ),
                'attr' => array(
                    'class' => 'image_type',
                ),
                'label' => false,
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data'     => self::URL_TYPE,
            ))
            ->add('url', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'class' => 'text_block',
                ),
                'required' => false,
            ))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context'  => 'default',
                'label' => false,
                'attr' => array(
                    'class' => 'media_block',
                    'style' => 'display:none',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\GoalImage',
        ));
    }
}