<?php

namespace AppBundle\Form\Type\Goal;

use ApiBundle\Entity\Goal;
use ApiBundle\Entity\GoalTarget;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CreateGoalFormType extends AbstractType
{
    const PUBLIC_ACCESS = 1;
    const PROTECTED_ACCESS = 2;
    const PRIVATE_ACCESS = 3;

    private $targets;

    public function __construct($options)
    {
        $this->targets = $options['targets'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', TextType::class, array(
                'label' => 'goal.url',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('title', TextType::class, array(
                'label' => 'goal.title',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('images', CollectionType::class, array(
                'entry_type' => GoalImagesType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            ))
            ->add('price', IntegerType::class, array(
                'label' => 'goal.price',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'goal.description',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('access', ChoiceType::class, array(
                'choices' => array(
                    self::PUBLIC_ACCESS => 'goal.public_access' ,
                    self::PROTECTED_ACCESS => 'goal.protected_access',
                    self::PRIVATE_ACCESS => 'goal.private_access',
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data'     => self::PUBLIC_ACCESS,
            ))
            ->add('create', SubmitType::class, array(
                'label' => 'goal.create',
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
            ))
            ->add('date_finish', DateTimeType::class, array(
                'label' => 'goal.finish_date',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('receiver', ChoiceType::class, array(
                'choices' => array(
                    Goal::FOR_ME => 'goal.for_me' ,
                    Goal::FOR_FRIEND => 'goal.for_friend',
                ),
                'label' => 'goal.receiver',
                'multiple' => false,
                'data'     => Goal::FOR_ME,
                'attr' => array(
                    'class' => 'mdb-select colorful-select dropdown-primary receiver_dropdown'
                ),
            ))
            ->add('target', EntityType::class, array(
                'label' => 'goal.target',
                'choices' => $this->targets,
                'multiple' => false,
//                'allow_add' => true,
                'class' => 'ApiBundle:GoalTarget',
                'attr' => array(
                    'class' => 'mdb-select colorful-select dropdown-primary target_dropdown'
                ),
//                'mapped' => false,
            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
            'data_class' => 'ApiBundle\Entity\Goal',
        ));
    }
}