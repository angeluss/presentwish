<?php

namespace AppBundle\Form\Type\Goal;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditGoalFormType extends CreateGoalFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('create');
        $builder->add('edit', SubmitType::class, array(
            'label' => 'goal_edit.submit',
            'attr' => array(
                'class' => 'btn btn-primary'
            ),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
        ));
    }
}