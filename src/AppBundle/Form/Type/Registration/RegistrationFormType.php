<?php

namespace AppBundle\Form\Type\Registration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'registration.email',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'registration.password',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('create', SubmitType::class, array(
                'label' => 'registration.create',
                'attr' => array(
                    'class' => 'btn btn-primary btn-lg'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
        ));
    }
}
