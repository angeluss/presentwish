<?php

namespace AppBundle\Form\Type\Registration;

use AppBundle\Form\Type\Registration\RegistrationFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SignUpFormType extends RegistrationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('create');
        $builder->add('signup', SubmitType::class, array(
            'label' => 'registration.signup',
            'attr' => array(
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'app',
        ));
    }
}