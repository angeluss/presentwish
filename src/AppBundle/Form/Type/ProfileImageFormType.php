<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProfileImageFormType extends AbstractType
{
    const FILE_TYPE = 1;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('get_from', ChoiceType::class, array(
                'choices' => array(
                    self::FILE_TYPE => 'profile.via_fileInput',
                ),
                'attr' => array(
                    'class' => 'image_type',
                ),
                'label' => false,
                'multiple' => false,
                'expanded' => false,
                'required' => true,
                'data'     => self::FILE_TYPE,
                'translation_domain' => 'app'
            ))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context'  => 'default',
                'label' => false,
                'attr' => array(
                    'class' => 'media_block',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\UserImage',
        ));
    }
}