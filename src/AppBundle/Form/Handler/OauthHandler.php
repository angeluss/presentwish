<?php
/**
 * Created by PhpStorm.
 * User: sandro
 * Date: 8/19/16
 * Time: 6:21 PM
 */

namespace AppBundle\Form\Handler;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use HWI\Bundle\OAuthBundle\Form\FOSUBRegistrationFormHandler;

class OauthHandler extends FOSUBRegistrationFormHandler
{
    /**
     * {@inheritDoc}
     */
    public function process(Request $request, Form $form, UserResponseInterface $userInformation)
    {
        if (null !== $this->registrationFormHandler) {
            $formHandler = $this->reconstructFormHandler($request, $form);

            // make FOSUB process the form already
            $processed = $formHandler->process();

            // if the form is not posted we'll try to set some properties
            if (!$request->isMethod('POST')) {
                $form->setData($this->setUserInformation($form->getData(), $userInformation));
            }

            return $processed;
        }

        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $form->setData($this->setUserInformation($user, $userInformation));

        if ($request->isMethod('POST')) {
            $form->bind($request);

            return $form->isValid();
        }

        return false;
    }
}