#!/usr/bin/env bash
sudo chmod 777 -R app/cache/dev
sudo chmod 777 -R app/cache/prod

php app/console cache:clear
php app/console doctrine:cache:clear-metadata
php app/console doctrine:cache:clear-query
php app/console doctrine:cache:clear-result

php app/console cache:clear --env=prod
php app/console doctrine:cache:clear-metadata --env=prod
php app/console doctrine:cache:clear-query --env=prod
php app/console doctrine:cache:clear-result --env=prod

sudo chmod 777 -R app/cache/dev
sudo chmod 777 -R app/cache/prod