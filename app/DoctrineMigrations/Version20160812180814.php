<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160812180814 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT INTO goal_target (title, type) VALUES ('birthday', 1)");
        $this->addSql("INSERT INTO goal_target (title, type) VALUES ('wedding', 1)");
        $this->addSql("INSERT INTO goal_target (title, type) VALUES ('new_year', 1)");
        $this->addSql("INSERT INTO goal_target (title, type) VALUES ('other', 1)");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM goal_target WHERE title=\'birthday\'');
        $this->addSql('DELETE FROM goal_target WHERE title=\'wedding\'');
        $this->addSql('DELETE FROM goal_target WHERE title=\'new_year\'');
        $this->addSql('DELETE FROM goal_target WHERE title=\'other\'');

    }
}
