//gulp requirements
var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    include = require('gulp-include'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    less = require('gulp-less'),
    path = require('path');

var errorMessage = {
    message: "<%= error.message %>",
    title: "ERROR"
};

var assetLocation = 'src/AppBundle/Resources/public/',
    outputAssetLocation = 'web/assets/';

var paths = {
    allImages: [
        assetLocation + 'images/**/*.{png,jpg,jpeg,gif,svg}'
    ],
    scripts: [
        assetLocation + 'scripts/**/*.js'
    ],
    scriptsCustom: assetLocation + 'scripts/app.js',
    styles: assetLocation + 'styles/*.{less,css}'
};

var imageminOptions = {
    optimizationLevel: 3,
    progressive: true,
    interlaced: true
};

var lrOptions = {
    // host: 'localhost',
    // port: 8080,
    //start: true,
    //quiet: true
};

gulp.task('default', function() {
    gulp.src(assetLocation + '*').pipe(gulp.dest('public'));
    return gutil.log('Gulp is running!')
});

// =============================================================================
// IMAGES
// =============================================================================
gulp.task('compressImages', function () {
    return gulp.src(paths.allImages)
        .pipe(plumber())
        .pipe(imagemin( imageminOptions ))
        .on("error", notify.onError(errorMessage))
        .pipe(gulp.dest(outputAssetLocation +  'img/'))
        .pipe(livereload(lrOptions));
});

// =============================================================================
// SCRIPTS
// =============================================================================
gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(plumber())
        //.pipe(concat('all.js'))
        //.pipe(uglify())
        .on("error", notify.onError(errorMessage))
        .pipe(gulp.dest(outputAssetLocation +  'scripts/'))
        .pipe(livereload(lrOptions));
});


gulp.task('scriptsCustom', function () {
    return gulp.src(paths.scriptsCustom)
        .pipe(plumber())
        .pipe(include())
        .on("error", notify.onError(errorMessage))
        .pipe(sourcemaps.init())
        .on("error", notify.onError(errorMessage))
        .pipe(babel())
        .on("error", notify.onError(errorMessage))
        .pipe(concat('app.js'))
        //.on("error", notify.onError(errorMessage))
        .pipe(sourcemaps.write('.'))
        .on("error", notify.onError(errorMessage))
        //.pipe(uglify())
        //.on("error", notify.onError(errorMessage))
        .pipe(gulp.dest(outputAssetLocation +  'scripts/'))
        .pipe(livereload(lrOptions));
});


// =============================================================================
// STYLES
// =============================================================================
gulp.task('styles', function () {
    return gulp.src(paths.styles)
        .pipe(plumber())
        .pipe(less())
        .on("error", notify.onError(errorMessage))
        .pipe(gulp.dest(outputAssetLocation + 'styles/'))
        .pipe(livereload(lrOptions));
});


// =============================================================================
// WATCHERS
// =============================================================================
gulp.task('watch', function() {
    livereload.listen(lrOptions);
    //GULP 4
    //watch(paths.markup, gulp.parallel('markup'));
    //watch(paths.scripts, gulp.parallel('scripts'));
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch([paths.scriptsCustom, assetLocation + 'scripts/components/*.js', assetLocation + 'scripts/models/*.js'], ['scriptsCustom']);
    gulp.watch(paths.allImages, ['compressImages']);
});

gulp.task('copyAssets', function () {
    gulp.src(assetLocation + 'fonts/*.*')
        .pipe(plumber())
        .pipe(gulp.dest(outputAssetLocation + 'fonts/'));
});

gulp.task('basic', ['styles', 'scripts', 'scriptsCustom', 'compressImages', 'copyAssets']);
gulp.task('compile', ['basic']);
gulp.task('default', ['basic', 'watch']);
